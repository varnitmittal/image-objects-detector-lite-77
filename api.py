from flask import Flask, request, render_template, jsonify, send_file
from imageai.Detection import ObjectDetection
from PIL import Image
import numpy as np
import base64
import io
import os
execution_path = os.getcwd()

app = Flask(__name__)

def get_model():
    global model
    model = ObjectDetection()
    model.setModelTypeAsRetinaNet()
    model.setModelPath( os.path.join(execution_path, './models/resnet50_coco_best_v2.0.1.h5'))
    model.loadModel()
    print("Model Loaded!!")

@app.route("/", methods=['GET'])
def index():
    return render_template("home.html")

@app.route("/predict", methods=["POST"])
def predict():
    final_result = []
#   image = request.files["file"].read()
    image = request.form['chosenImage']
    image = base64.b64decode(image)
    image = Image.open(io.BytesIO(image))
    if image.mode != "RGB":
        image = image.convert("RGB")
    image_array = np.asarray(image, dtype="float32")
    detections = model.detectObjectsFromImage(
        input_type = "array",
        input_image = image_array,
        output_image_path=os.path.join(execution_path , "myimage.jpg")
    )
    for eachObject in detections:
        final_result.append({"object": eachObject["name"],"probability": eachObject["percentage_probability"]})

    with open("myimage.jpg", "rb") as f:
        x = f.read()
        x = base64.b64encode(x).decode('utf-8')

    return jsonify({
        "success": True,
        "imstr": x,
        "detections": final_result,
    })

get_model()
if __name__ == "__main__":
    port = int(os.environ.get('PORT', 33507))
    app.run(port=port)
